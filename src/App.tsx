import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import Home from "./pages/Home";
import Mongo from "./pages/Mongo";
import NotFound from "./pages/NotFound";
import Head from "./components/Layout/Head";
import TodoList from "./pages/TodoList";

function App() {
  return (
    <>
      <Head />
      <Router>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/mongo' component={Mongo} />
          <Route path='/todoList' component={TodoList} />
          <Route path='/notFound' component={NotFound} />
          <Redirect to='/notFound' />
        </Switch>
      </Router>
    </>
  );
}

export default App;
