import { get, post } from "./index";
import { GET, POST } from "./types";

export const addTodo = async (data: {
  content: string;
  reminderTime: Date;
}) => {
  const result = await post(POST.addTodo, data);
  return result?.data;
};

export const getTodoLists = async () => {
  const result = await get(GET.getTodoLists);
  return result?.data;
};

export interface IToggleTodoStatus {
  id: string;
  status: boolean;
}

export const toggleTodoStatus = async (data: IToggleTodoStatus) => {
  const result = await post(POST.toggleTodoState, data);
  return result?.data;
};

export interface IDeleteTodoItem {
  id: string;
}

export const deleteTodoItem = async (data: IDeleteTodoItem) => {
  const result = await post(POST.deleteTodoItem, data);
  return result?.data;
};

export const getUserProfiles = async () => {
  const result = await get(GET.getUserProfiles);
  return result?.data;
};

export const getDBUrl = async () => {
  const result = await get(GET.DB_URL);
  return result?.data;
};

export const MongoQuery = async (query: string) => {
  const result = await get(GET.MongoQuery, { params: { query } });
  return result?.data;
};
