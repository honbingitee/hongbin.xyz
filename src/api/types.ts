export enum GET {
  DB_URL = "/dbUrl",
  MongoQuery = "/MongoQuery",
  getUserProfiles = "/getUserProfiles",
  getTodoLists = "getTodoLists",
}

export enum POST {
  addTodo = "addTodo",
  toggleTodoState = "toggleTodoState",
  deleteTodoItem = "deleteTodoItem",
}
