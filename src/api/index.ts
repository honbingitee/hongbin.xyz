import axios, { AxiosResponse, AxiosRequestConfig } from "axios";
import { GET, POST } from "./types";
// const baseURL = `${process.env.REACT_APP_URL}:${process.env.REACT_APP_PORT}`;
const baseURL = `${process.env.REACT_APP_URL}`;
axios.defaults.baseURL = baseURL;

// type GETType = (
//   path: GET,
//   params?: AxiosRequestConfig
// ) => Promise<AxiosResponse<GET>>;
type GETType = (
  path: GET,
  config?: AxiosRequestConfig | undefined
) => Promise<AxiosResponse<any>>;

export const get: GETType = async (path, config) => {
  return await axios.get(`${path}`, config);
};

type POSTType = (
  path: POST,
  data?: any,
  config?: AxiosRequestConfig | undefined
) => Promise<AxiosResponse<any>>;

export const post: POSTType = async (path, data, config) => {
  return await axios.post(`${path}`, data, config);
};
