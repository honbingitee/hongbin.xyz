import { FC, ReactElement, useState, useMemo, useReducer } from "react";
import { Headline } from "../../components/BUI";
import { reducer } from "../../components/TodoList/reducer";
import { fetchTodoList } from "../../components/TodoList/actions";
import { useMount } from "../../hooks";
import TodoInput from "../../components/TodoList/TodoInput";
import TodoLists from "../../components/TodoList/TodoLists";
import styled from "styled-components";
import { TodoList as TodoListType } from "../../components/TodoList/types";

interface IProps {}

const TodoList: FC<IProps> = (): ReactElement => {
  const [todoLists, dispatch] = useReducer(reducer, []);
  const [drag, setDrag] = useState(false);
  const [todoList, doneList] = useMemo(() => {
    const todo: TodoListType = [];
    const done: TodoListType = [];
    todoLists.forEach(item => {
      if (item.status) {
        done.push(item);
      } else todo.push(item);
    });
    return [todo, done];
  }, [todoLists]);

  useMount(() => fetchTodoList()(dispatch));

  return (
    <Container>
      <TodoContainer>
        <Headline color='#fff'>TodoList</Headline>
        <TodoInput dispatch={dispatch} />
        <TodoLists
          dispatch={dispatch}
          todoLists={todoList}
          drag={drag}
          setDrag={setDrag}
        />
      </TodoContainer>
      {/* <DoneLists dispatch={dispatch} doneLists={doneList} /> */}
      <TodoContainer>
        <Headline>DoneList</Headline>
        <TodoLists
          dispatch={dispatch}
          todoLists={doneList}
          done={true}
          drag={drag}
          setDrag={setDrag}
        />
      </TodoContainer>
    </Container>
  );
};

export default TodoList;

const TodoContainer = styled.div`
  height: 100%;
  width: 20rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  width: 40rem;
  background-color: #ccc;
  box-shadow: 3px 0 8px 3px #eee;
  margin: 0 auto;
  height: calc(100% - 4rem);
`;
