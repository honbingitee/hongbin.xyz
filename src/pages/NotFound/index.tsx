import { FC, ReactElement } from "react";

interface IProps {}

const NotFound: FC<IProps> = (): ReactElement => {
  return <div>NotFound</div>;
};

export default NotFound;
