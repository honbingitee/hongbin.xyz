import { FC, ReactElement } from "react";

interface IProps {}

const Home: FC<IProps> = (): ReactElement => {
  return <div>Home</div>;
};

export default Home;
