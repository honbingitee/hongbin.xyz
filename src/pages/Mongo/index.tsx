import { FC, ReactElement, useCallback, useRef } from "react";
import { getUserProfiles, MongoQuery } from "../../api/mongoAPI";
import { Button, CenterBox, Textarea } from "../../components/BUI";

interface IProps {}

const Mongo: FC<IProps> = (): ReactElement => {
  const input = useRef<HTMLTextAreaElement | null>(null);

  const handleTestMongo = useCallback(() => {
    const value = input.current?.value;
    if (value) {
      MongoQuery(value)
        .then(res => {
          console.log("res", res);
        })
        .catch(err => {
          console.log("err", err);
        });
      console.log(value);
    } else alert("没有数据测试你妹");
  }, [input]);

  return (
    <CenterBox>
      <Textarea ref={input} />
      <Button onClick={handleTestMongo} margin>
        submit
      </Button>
      <Button
        onClick={async () => {
          const users = await getUserProfiles();
          console.log(users);
        }}
        margin
      >
        getUserProfiles
      </Button>
    </CenterBox>
  );
};

export default Mongo;
