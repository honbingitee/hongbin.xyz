import { IToggleTodoStatus } from "../../api/mongoAPI";

export type Content = string;
export type ID = string;

export interface Todo {
  _id: ID;
  content: Content;
  status: boolean;
  reminderTime: Date;
}

export type TodoList = Array<Todo>;

export enum Actions {
  FETCH_TODO_LIST = "FETCH_TODO_LIST",
  ADD_TODO = "ADD_TODO",
  REMOVE_TODO = "REMOVE_TODO",
  TOGGLE_TODO_STATUS = "TOGGLE_TODO_STATUS",
}

export interface IAction {
  type: Actions;
  payload?: ID | Todo | TodoList | IToggleTodoStatus;
}
