import styled from "styled-components";

const Button = styled.button<{ margin?: boolean; width?: string }>`
  width: ${props => props.width};
  background-color: #51f;
  color: #fff;
  font-size: 0.7rem;
  border-radius: 0.25rem;
  border: none;
  text-transform: capitalize;
  height: 2rem;
  /* padding: 0.2rem 0.7rem; */
  font-weight: bold;
  margin: ${props => (props.margin ? "0 0.5rem" : "none")};
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`;

export default Button;
