import styled from "styled-components";
import { AlignItems, TPosition } from "../types";

const CenterBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export default CenterBox;

export const ItemCenterBox = styled.div`
  display: flex;
  align-items: center;
`;

export const BetweenBox = styled.div<{
  width?: string;
  height?: string;
  position?: TPosition;
  dir?: "row" | "column";
  item?: AlignItems;
}>`
  display: flex;
  flex-direction: ${({ dir }) => dir};
  justify-content: space-between;
  align-items: ${props => props.item || "center"};
  width: ${props => props.width};
  height: ${props => props.height};
  position: ${props => props.position};
`;
