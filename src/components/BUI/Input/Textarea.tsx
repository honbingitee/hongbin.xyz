import styled from "styled-components";

const Textarea = styled.textarea`
  padding: 0.3rem;
  border: none;
  border-top-left-radius: 0.2rem;
  border-top-right-radius: 0.3rem;
  background-color: #b8b3b360;
  border-bottom: 0.1rem solid #51f;
  min-height: 5rem;
  max-height: 8rem;
  width: 20rem;
  outline-color: #51f;
`;
// const Textarea = styled.textarea(() => ({

// }))

export default Textarea;
