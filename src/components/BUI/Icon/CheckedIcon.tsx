import styled from "styled-components";

const CheckedIcon = styled.svg.attrs({
  children: (
    <path
      d='M816 96a128 128 0 0 1 128 128v592a128 128 0 0 1-128 128H224a128 128 0 0 1-128-128V224a128 128 0 0 1 128-128h592z m-69.632 182.464l-333.152 333.12-146.24-146.24-66.624 66.624 212.896 212.896 399.744-399.776-66.624-66.624z'
      p-id='2686'
      fill='#cccccc'
    ></path>
  ),
  viewBox: "0 0 1024 1024",
  version: "1.1",
  xmlns: "http://www.w3.org/2000/svg",
  "p-id": "2685",
  width: "1rem",
  height: "1rem",
})`
  ${(props: { status?: boolean }) =>
    props.status
      ? `path {
    fill: #51f;
  }`
      : undefined}
`;

export default CheckedIcon;
