import styled from "styled-components";

const DeleteIcon = styled.div.attrs({
  children: (
    <svg
      viewBox='0 0 1024 1024'
      version='1.1'
      xmlns='http://www.w3.org/2000/svg'
      p-id='4823'
      width='1rem'
      height='1rem'
    >
      <path
        d='M661.333333 490.666667H362.666667a21.333333 21.333333 0 0 0 0 42.666666h298.666666a21.333333 21.333333 0 0 0 0-42.666666z'
        fill='#2c2c2c'
        p-id='4824'
      ></path>
      <path
        d='M512 85.333333a426.666667 426.666667 0 1 0 426.666667 426.666667A427.157333 427.157333 0 0 0 512 85.333333z m0 810.666667a384 384 0 1 1 384-384 384.426667 384.426667 0 0 1-384 384z'
        fill='#2c2c2c'
        p-id='4825'
      ></path>
    </svg>
  ),
})`
  cursor: pointer;
  width: 1rem;
  height: 1rem;

  &:hover {
    svg > path {
      fill: #51f;
    }
  }
`;

export default DeleteIcon;
