import { FC, ReactElement, useCallback, useMemo } from "react";
import { SubHead } from "../Text";
import { AbsolutePopup } from "./styled";

interface IProps {
  active: boolean;
  month: number;
  selectMonth: React.Dispatch<React.SetStateAction<number>>;
}

const MonthSelected: FC<IProps> = ({
  active,
  month,
  selectMonth,
}): ReactElement => {
  const months = useMemo(
    () => [
      "一月",
      "二月",
      "三月",
      "四月",
      "五月",
      "六月",
      "七月",
      "八月",
      "九月",
      "十月",
      "十一月",
      "十二月",
    ],
    []
  );

  const handleSelectMonth = useCallback(
    month => () => {
      selectMonth(month);
    },
    [selectMonth]
  );

  const renderItem = useCallback(
    (m, index) => {
      const props = {
        onClick: handleSelectMonth(index + 1),
        key: index,
        children: m,
      };
      if (index === month - 1) return <ActiveMonth {...props} />;
      return <Month {...props} />;
    },
    [handleSelectMonth, month]
  );

  return (
    <AbsolutePopup bottom={active ? 0 : "100%"}>
      {months.map(renderItem)}
    </AbsolutePopup>
  );
};

export default MonthSelected;

const Month = ({ style, ...props }: any) => (
  <SubHead
    style={{
      width: "33%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      ...style,
    }}
    {...props}
  />
);

const ActiveMonth = ({ ...props }: any) => (
  <Month
    style={{
      backgroundColor: "#51f",
      color: "#fff",
    }}
    {...props}
  />
);
