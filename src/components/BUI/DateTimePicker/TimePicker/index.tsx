import React, {
  FC,
  ReactElement,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import { ShrinkIcon, MikeTrueIcon, Wrapper } from "../styled";
import { animateScrollToTop } from "../utils";

interface IProps {
  status: boolean;
  defaultHour: number;
  defaultMinutes: number;
  selectedTime: ({ hour, minutes }: { [key: string]: number }) => void;
  cancelTimePicker: () => void;
}

const hourArr = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23,
];

const minutesArr = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
  41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
  60,
];

const TimePicker: FC<IProps> = ({
  status,
  defaultHour,
  defaultMinutes,
  selectedTime,
  cancelTimePicker,
}): ReactElement => {
  const [hidden, setHidden] = useState(!status);
  const [hour, setHour] = useState(defaultHour);
  const [minutes, setMinutes] = useState(defaultMinutes);
  const hourRef = useRef<HTMLSpanElement | null>(null);
  const MinutesRef = useRef<HTMLSpanElement | null>(null);

  const handleSelectedTime = useCallback(() => {
    selectedTime({ hour, minutes });
  }, [hour, minutes, selectedTime]);

  const scrollToTop = useCallback(
    ({ offsetTop, parentNode }) => animateScrollToTop(offsetTop, parentNode)(),
    []
  );
  // 添加消失动画 看起来舒服些😌
  useEffect(() => {
    if (!status) {
      setTimeout(() => {
        setHidden(true);
      }, 200);
    } else setHidden(false);
  }, [status]);
  //  第一次进入时 选中的小时 滚动到最上方
  useEffect(() => {
    if (status && hourRef.current) {
      //元素未出现时 滚动无效 setTimeout为了让 父元素出现后有效滚动
      setTimeout(() => {
        scrollToTop(hourRef.current);
      }, 0);
    }
  }, [hourRef, scrollToTop, status]);
  //  第一次进入时 选中的分钟 滚动到最上方
  useEffect(() => {
    if (status && MinutesRef.current) {
      setTimeout(() => {
        scrollToTop(MinutesRef.current);
      }, 0);
    }
  }, [MinutesRef, scrollToTop, status]);

  const select = useCallback(
    ({ target }: any | HTMLElement) => {
      const type = target.getAttribute("type");
      const value = Number(target.getAttribute("value"));
      if (type === "HOUR") setHour(value);
      else setMinutes(value);
      scrollToTop(target);
    },
    [scrollToTop]
  );

  return (
    <Container hidden={hidden} status={status}>
      <Wrapper style={{ alignItems: "center" }} width='8rem' row>
        <Line>
          {hourArr.map(h => {
            const props = {
              onClick: select,
              key: h,
              children: h,
              type: "HOUR",
              value: h,
            };
            if (h === hour) return <ActiveItem ref={hourRef} {...props} />;
            return <Item {...props} />;
          })}
        </Line>
        <VerticalLine />
        <Line>
          {minutesArr.map(m => {
            const props = {
              onClick: select,
              key: m,
              children: m,
              type: "MINUTES",
              value: m,
            };
            if (m === minutes)
              return <ActiveItem ref={MinutesRef} {...props} />;
            return <Item {...props} />;
          })}
        </Line>
      </Wrapper>
      <IconBox onClick={cancelTimePicker}>
        <ShrinkIcon />
      </IconBox>
      <IconBox onClick={handleSelectedTime}>
        <MikeTrueIcon />
      </IconBox>
    </Container>
  );
};

export default TimePicker;

const Container = styled(Wrapper).attrs({ noShadow: true, row: false })<{
  hidden?: boolean;
  status?: boolean;
}>`
  background-color: transparent;
  backdrop-filter: blur(0.4rem);
  display: ${({ hidden }) => (hidden ? "none" : "flex")};
  align-items: flex-end;
  justify-content: flex-end;
  opacity: ${({ status }) => (status ? 1 : 0)};
  transition: opacity 0.2s linear;
`;

const IconBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 3rem;
  height: 1.5rem;
  cursor: pointer;
  border-radius: 0.15rem;
  margin: 0.1rem;
  margin-right: 0;
  border: 1px dashed #ccc;
  transition-property: background-color, box-shadow, border-color;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  background-color: #fff;
  box-shadow: inset 5px 5px 13px #ededed, inset -5px -5px 13px #ffffff;

  &:hover {
    background-color: #51f;
    box-shadow: 0 0 0 0 #51f;
    border-color: transparent;

    svg > path {
      fill: #fff !important;
    }
  }
`;

const Line = styled.div`
  display: flex;
  height: 14rem;
  flex-wrap: wrap;
  flex: 1;
  overflow-y: scroll;
  padding-bottom: 12rem;
  position: relative;
`;

const Item = styled.span.attrs(({ type, value }: any) => ({ type, value }))<{
  type: "HOUR" | "MINUTES";
  value: number;
}>`
  width: 100%;
  height: 2rem;
  text-align: center;
  line-height: 2rem;
  transition: background-color 0.2s linear;

  &:hover {
    background-color: #ccc;
  }
`;

const ActiveItem = styled.span`
  width: 100%;
  height: 2rem;
  line-height: 2rem;
  text-align: center;
  transition: background-color 0.2s linear;
  background-color: #51f;
  color: #fff;
  font-weight: bold;
`;

const VerticalLine = styled.div`
  height: 90%;
  width: 0.0625rem;
  background-color: #ccc;
  margin: 0 0.2rem;
`;
