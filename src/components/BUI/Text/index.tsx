import styled from "styled-components";

export const Headline = styled.p<{ shadow?: boolean; color?: string }>`
  font-weight: bold;
  font-size: 1.5rem;
  color: ${props => props.color};
  ${props => (props.shadow ? "text-shadow: 0 0 5px #ccc;" : undefined)}
`;

export const Caption = styled.p<{ size?: string; color?: string }>`
  font-size: ${({ size }) => size || "0.5rem"};
  margin: 0;
  color: ${props => props.color || "#333"};
  transition-property: letter-spacing, color;
  transition-duration: 0.5s;
  transition-timing-function: linear;
`;

export const SubHead = styled.p<{ width?: string; height?: string }>`
  font-size: 0.625rem;
  margin: 0;
  font-weight: bold;
  min-width: ${props => props.width};
  min-height: ${props => props.height};
  line-height: ${props => props.height};
  text-align: center;
  transition: background-color 0.3s linear;
  border-radius: 0.1rem;
  &:hover {
    background-color: #cccccc7a;
  }
`;
