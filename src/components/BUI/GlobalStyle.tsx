import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,body,#root {
    height: 100%;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  }
  div,nav{
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }
`;

export default GlobalStyle;
