import styled from "styled-components";

const Divider = styled.div<{ width?: string; height?: string; color?: string }>`
  width: ${props => props.width || "100%"};
  height: ${props => props.height || "0.0625rem"};
  background-color: ${props => props.color || "#dfdbdbb9"};
  margin: 0.25rem auto;
`;

export default Divider;
