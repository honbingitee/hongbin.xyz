import { FC, ReactElement, useState, useCallback } from "react";
import CheckedIcon from "../Icon/CheckedIcon";
import { Box } from "../styled";

interface IProps {
  checked: boolean;
  onClick: (status: boolean) => void;
}

const CheckBox: FC<IProps> = ({ onClick, checked }): ReactElement => {
  const [status, setStatus] = useState<boolean>(checked);

  const handleSwitchStatus = useCallback(() => {
    setStatus(state => !state);
    onClick(!status);
  }, [onClick, status]);

  return (
    <Box onClick={handleSwitchStatus}>
      <CheckedIcon status={status} />
    </Box>
  );
};

export default CheckBox;
